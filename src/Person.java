

public class Person {

	private String eventName;
	private String scannedDate;
	private String scannedTime;
	private String firstName;
	private String middleInitial;
	private String lastName;
	private String email;
	private String email2;
	private String companyName;
	private String jobTitle;
	private String address;
	private String address2;
	private String address3;
	private String city;
	private String stateId;
	private String zip;
	private String countryId;
	private String phoneNumber;
	private String faxNumber;
	private String question;
	private String response;
	private String note;
	private String collateral;
	private String qd;
	private String scannedBy;
	
	public Person() {
		// TODO Auto-generated constructor stub
	}

	public Person(String eventName, String scannedDate, String scannedTime, String firstName, String middleInitial,
			String lastName, String email, String email2, String companyName, String jobTitle, String address,
			String address2, String address3, String city, String stateId, String zip, String countryId,
			String phoneNumber, String faxNumber, String question, String response, String note, String collateral,
			String qd, String scannedBy) {
		super();
		this.eventName = eventName;
		this.scannedDate = scannedDate;
		this.scannedTime = scannedTime;
		this.firstName = firstName;
		this.middleInitial = middleInitial;
		this.lastName = lastName;
		this.email = email;
		this.email2 = email2;
		this.companyName = companyName;
		this.jobTitle = jobTitle;
		this.address = address;
		this.address2 = address2;
		this.address3 = address3;
		this.city = city;
		this.stateId = stateId;
		this.zip = zip;
		this.countryId = countryId;
		this.phoneNumber = phoneNumber;
		this.faxNumber = faxNumber;
		this.question = question;
		this.response = response;
		this.note = note;
		this.collateral = collateral;
		this.qd = qd;
		this.scannedBy = scannedBy;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getScannedDate() {
		return scannedDate;
	}

	public void setScannedDate(String scannedDate) {
		this.scannedDate = scannedDate;
	}

	public String getScannedTime() {
		return scannedTime;
	}

	public void setScannedTime(String scannedTime) {
		this.scannedTime = scannedTime;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCollateral() {
		return collateral;
	}

	public void setCollateral(String collateral) {
		this.collateral = collateral;
	}

	public String getQd() {
		return qd;
	}

	public void setQd(String qd) {
		this.qd = qd;
	}

	public String getScannedBy() {
		return scannedBy;
	}

	public void setScannedBy(String scannedBy) {
		this.scannedBy = scannedBy;
	}
	
	
}
