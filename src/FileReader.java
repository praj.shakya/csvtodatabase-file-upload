import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class FileReader {

	public static void main(String[] args) throws ClassNotFoundException {
		String jdbcUrl = "jdbc:mysql://localhost/db_unlimited?serverTimezone=UTC";
		String jdbcName="root";
		String jdbcPwd="";
		String csvFile="C:\\Users\\User\\eclipse-workspace\\CsvFileReader\\persons.csv";
		
		int batchSize = 300;
		
		Connection connection=null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(jdbcUrl, jdbcName, jdbcPwd);
			connection.setAutoCommit(false);
			String sql = "INSERT INTO tbl_person (eventName, scannedDate, scannedTime, firstName, middleInitial, "
					+ "lastName, email, email2, companyName, jobTitle, address, address2, address3, city, stateId,"
					+ " zip, countryId, phoneNumber, phoneNumber2, faxNumber, question, response, note, collateral,"
					+ " qd, scannedBy) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			//String sql="insert into tbl_employee(name,address,phone) values(?,?,?)";
			PreparedStatement statment=connection.prepareStatement(sql);
			BufferedReader reader=new BufferedReader(new java.io.FileReader(csvFile));
			
			String line="";
			int count = 0;
			
			reader.readLine();
			while ((line = reader.readLine()) != null) {
				String [] data = line.split(",");
				
				if(data.length>=26) {
				
				String eventName = data[0];
				String scannedDate = data[1];
				String scannedTime = data[2];
				String firstName = data[3];
				String middleInitial = data[4];
				String lastName = data[5];
				String email = data[6];
				String email2 = data[7];
				String companyName = data[8];
				String jobTitle = data[9];
				String address = data[10];
				String address2 = data[11];
				String address3 = data[12];
				String city = data[13];
				String stateId = data[14];
				String zip = data[15];
				String countryId = data[16];
				String phoneNumber = data[17];
				String phoneNumber2 = data[18];
				String faxNumber = data[19];
				String question = data[20];
				String response = data[21];
				String note = data[22];
				String collateral = data[23];
				String qd = data[24];
				String scannedBy = data[25];
				
				statment.setString(1, eventName);
				statment.setString(2, scannedDate);
				statment.setString(3, scannedTime);
				statment.setString(4, firstName);
				statment.setString(5, middleInitial);
				statment.setString(6, lastName);
				statment.setString(7, email);
				statment.setString(8, email2);
				statment.setString(9, companyName);
				statment.setString(10, jobTitle);
				statment.setString(11, address);
				statment.setString(12, address2);
				statment.setString(13, address3);
				statment.setString(14, city);
				statment.setString(15, stateId);
				statment.setString(16, zip);
				statment.setString(17, countryId);
				statment.setString(18, phoneNumber);
				statment.setString(19, phoneNumber2);
				statment.setString(20, faxNumber);
				statment.setString(21, question);
				statment.setString(22, response);
				statment.setString(23, note);
				statment.setString(24, collateral);
				statment.setString(25, qd);
				statment.setString(26, scannedBy);
				
				
				statment.addBatch();
				
				if(count % batchSize == 0) {
					statment.executeBatch();
				
				}
				}
			}
			reader.close();
			 
            
            statment.executeBatch();
 
            connection.commit();
            System.out.println("File Uploaded to database succcessfully");
            connection.close();
			
		}catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}catch (SQLException sqle) {
			System.out.println(sqle.getMessage());
			
			try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
		}
	}

}
